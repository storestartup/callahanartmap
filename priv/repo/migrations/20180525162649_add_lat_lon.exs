defmodule Callahanartmap.Repo.Migrations.AddLatLon do
  use Ecto.Migration

  def change do
      alter table(:murals) do
        add :lat, :float
        add :lon, :float
      end
  end
end
