defmodule Callahanartmap.Repo.Migrations.CreateMurals do
  use Ecto.Migration

  def change do
    create table(:murals) do
      add :name, :string
      add :description, :text
      add :location_name, :string
      add :address, :string
      add :city, :string
      add :state, :string
      add :zip, :string

      timestamps()
    end

  end
end
