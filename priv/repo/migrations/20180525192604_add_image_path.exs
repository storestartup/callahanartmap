defmodule Callahanartmap.Repo.Migrations.AddImagePath do
  use Ecto.Migration

  def change do
    alter table(:murals) do
      add :image_path, :string
    end
  end
end
