defmodule CallahanartmapWeb.MuralControllerTest do
  use CallahanartmapWeb.ConnCase

  alias Callahanartmap.Core

  @create_attrs %{address: "some address", city: "some city", description: "some description", location_name: "some location_name", name: "some name", state: "some state", zip: "some zip"}
  @update_attrs %{address: "some updated address", city: "some updated city", description: "some updated description", location_name: "some updated location_name", name: "some updated name", state: "some updated state", zip: "some updated zip"}
  @invalid_attrs %{address: nil, city: nil, description: nil, location_name: nil, name: nil, state: nil, zip: nil}

  def fixture(:mural) do
    {:ok, mural} = Core.create_mural(@create_attrs)
    mural
  end

  describe "index" do
    test "lists all murals", %{conn: conn} do
      conn = get conn, mural_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Murals"
    end
  end

  describe "new mural" do
    test "renders form", %{conn: conn} do
      conn = get conn, mural_path(conn, :new)
      assert html_response(conn, 200) =~ "New Mural"
    end
  end

  describe "create mural" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, mural_path(conn, :create), mural: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == mural_path(conn, :show, id)

      conn = get conn, mural_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Mural"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, mural_path(conn, :create), mural: @invalid_attrs
      assert html_response(conn, 200) =~ "New Mural"
    end
  end

  describe "edit mural" do
    setup [:create_mural]

    test "renders form for editing chosen mural", %{conn: conn, mural: mural} do
      conn = get conn, mural_path(conn, :edit, mural)
      assert html_response(conn, 200) =~ "Edit Mural"
    end
  end

  describe "update mural" do
    setup [:create_mural]

    test "redirects when data is valid", %{conn: conn, mural: mural} do
      conn = put conn, mural_path(conn, :update, mural), mural: @update_attrs
      assert redirected_to(conn) == mural_path(conn, :show, mural)

      conn = get conn, mural_path(conn, :show, mural)
      assert html_response(conn, 200) =~ "some updated address"
    end

    test "renders errors when data is invalid", %{conn: conn, mural: mural} do
      conn = put conn, mural_path(conn, :update, mural), mural: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Mural"
    end
  end

  describe "delete mural" do
    setup [:create_mural]

    test "deletes chosen mural", %{conn: conn, mural: mural} do
      conn = delete conn, mural_path(conn, :delete, mural)
      assert redirected_to(conn) == mural_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, mural_path(conn, :show, mural)
      end
    end
  end

  defp create_mural(_) do
    mural = fixture(:mural)
    {:ok, mural: mural}
  end
end
