defmodule Callahanartmap.CoreTest do
  use Callahanartmap.DataCase

  alias Callahanartmap.Core

  describe "murals" do
    alias Callahanartmap.Core.Mural

    @valid_attrs %{address: "some address", city: "some city", description: "some description", location_name: "some location_name", name: "some name", state: "some state", zip: "some zip"}
    @update_attrs %{address: "some updated address", city: "some updated city", description: "some updated description", location_name: "some updated location_name", name: "some updated name", state: "some updated state", zip: "some updated zip"}
    @invalid_attrs %{address: nil, city: nil, description: nil, location_name: nil, name: nil, state: nil, zip: nil}

    def mural_fixture(attrs \\ %{}) do
      {:ok, mural} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Core.create_mural()

      mural
    end

    test "list_murals/0 returns all murals" do
      mural = mural_fixture()
      assert Core.list_murals() == [mural]
    end

    test "get_mural!/1 returns the mural with given id" do
      mural = mural_fixture()
      assert Core.get_mural!(mural.id) == mural
    end

    test "create_mural/1 with valid data creates a mural" do
      assert {:ok, %Mural{} = mural} = Core.create_mural(@valid_attrs)
      assert mural.address == "some address"
      assert mural.city == "some city"
      assert mural.description == "some description"
      assert mural.location_name == "some location_name"
      assert mural.name == "some name"
      assert mural.state == "some state"
      assert mural.zip == "some zip"
    end

    test "create_mural/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Core.create_mural(@invalid_attrs)
    end

    test "update_mural/2 with valid data updates the mural" do
      mural = mural_fixture()
      assert {:ok, mural} = Core.update_mural(mural, @update_attrs)
      assert %Mural{} = mural
      assert mural.address == "some updated address"
      assert mural.city == "some updated city"
      assert mural.description == "some updated description"
      assert mural.location_name == "some updated location_name"
      assert mural.name == "some updated name"
      assert mural.state == "some updated state"
      assert mural.zip == "some updated zip"
    end

    test "update_mural/2 with invalid data returns error changeset" do
      mural = mural_fixture()
      assert {:error, %Ecto.Changeset{}} = Core.update_mural(mural, @invalid_attrs)
      assert mural == Core.get_mural!(mural.id)
    end

    test "delete_mural/1 deletes the mural" do
      mural = mural_fixture()
      assert {:ok, %Mural{}} = Core.delete_mural(mural)
      assert_raise Ecto.NoResultsError, fn -> Core.get_mural!(mural.id) end
    end

    test "change_mural/1 returns a mural changeset" do
      mural = mural_fixture()
      assert %Ecto.Changeset{} = Core.change_mural(mural)
    end
  end
end
