defmodule Callahanartmap.Core.Mural do
  use Ecto.Schema
  import Ecto.Changeset


  schema "murals" do
    field :address, :string
    field :city, :string
    field :description, :string
    field :location_name, :string
    field :name, :string
    field :state, :string
    field :zip, :string
    field :lat, :float
    field :lon, :float
    field :image_path, :string

    timestamps()
  end

  @doc false
  def changeset(mural, attrs) do
    mural
    |> cast(attrs, [:name, :description, :location_name, :address, :city,
      :state, :zip, :lat, :lon, :image_path])
    |> validate_required([:name, :location_name, :address, :city, :state, :zip])
  end
end
