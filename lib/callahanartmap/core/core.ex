defmodule Callahanartmap.Core do
  @moduledoc """
  The Core context.
  """

  import Ecto.Query, warn: false
  alias Callahanartmap.Repo

  alias Callahanartmap.Core.Mural

  require Logger

  @doc """
  Returns the list of murals.

  ## Examples

      iex> list_murals()
      [%Mural{}, ...]

  """
  def list_murals do
    Repo.all(Mural)
  end

  @doc """
  Gets a single mural.

  Raises `Ecto.NoResultsError` if the Mural does not exist.

  ## Examples

      iex> get_mural!(123)
      %Mural{}

      iex> get_mural!(456)
      ** (Ecto.NoResultsError)

  """
  def get_mural!(id), do: Repo.get!(Mural, id)

  @doc """
  Creates a mural. Will try to geocode the address

  ## Examples

      iex> create_mural(%{field: value})
      {:ok, %Mural{}}

      iex> create_mural(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_mural(attrs \\ %{}) do
    %Mural{}
    |> Mural.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a mural.

  ## Examples

      iex> update_mural(mural, %{field: new_value})
      {:ok, %Mural{}}

      iex> update_mural(mural, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_mural(%Mural{} = mural, attrs) do
    mural
    |> Mural.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Mural.

  ## Examples

      iex> delete_mural(mural)
      {:ok, %Mural{}}

      iex> delete_mural(mural)
      {:error, %Ecto.Changeset{}}

  """
  def delete_mural(%Mural{} = mural) do
    Repo.delete(mural)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking mural changes.

  ## Examples

      iex> change_mural(mural)
      %Ecto.Changeset{source: %Mural{}}

  """
  def change_mural(%Mural{} = mural) do
    Mural.changeset(mural, %{})
  end

  def geocode(%Mural{} = mural) do
    address = "#{mural.address} #{mural.city}, #{mural.state} #{mural.zip} USA"
    query_string = URI.encode_query(%{"address" => address, "key" => "AIzaSyDbfuRH_wG0PaSTZu-AUGiBHI3FY7QtmiQ"})
    case HTTPoison.get("https://maps.googleapis.com/maps/api/geocode/json?#{query_string}") do
      {:ok, %{status_code: 200, body: body}} ->
        case Jason.decode(body) do
          {:ok, json} ->
            case parse_geocode_result(json) do
              {:ok, {lat, lon}} ->
                Logger.info "geocoded #{mural.name} to #{lat},#{lon}"
                cs = Mural.changeset(mural, %{lat: lat, lon: lon})
                Repo.update(cs)
              bad_result ->
                Logger.error "Bad result from google geocoding: #{inspect bad_result}"
                bad_result
            end
          err ->
            Logger.error "Error decoding json result from google geocoding: #{inspect err}"
            err
        end
      bad ->
        Logger.error "Error getting result from google geocoding: #{inspect bad}"
        bad
    end
  end

  def parse_geocode_result(%{"status" => "OK", "results" => results}) do
    result = hd(results)
    {:ok, {result["geometry"]["location"]["lat"], result["geometry"]["location"]["lng"]}}
  end
  def parse_geocode_result(bad_result), do: {:error, bad_result}

end
