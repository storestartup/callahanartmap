defmodule CallahanartmapWeb.Router do
  use CallahanartmapWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CallahanartmapWeb do
    pipe_through :browser # Use the default browser stack

    get "/", MuralController, :index

    resources "/murals", MuralController

    post "/murals/:id/upload_image", MuralController, :upload_image
  end

  # Other scopes may use custom stacks.
  # scope "/api", CallahanartmapWeb do
  #   pipe_through :api
  # end
end
