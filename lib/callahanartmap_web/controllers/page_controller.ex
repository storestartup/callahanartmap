defmodule CallahanartmapWeb.PageController do
  use CallahanartmapWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
