defmodule CallahanartmapWeb.MuralController do
  use CallahanartmapWeb, :controller

  alias Callahanartmap.Core
  alias Callahanartmap.Core.Mural
  require Logger

  def upload_image(conn, %{"mural" => %{"photo" => upload}, "id" => id}) do
    mural = Core.get_mural!(id)
    Logger.debug "Received upload: #{inspect upload}"
    File.cp!(upload.path, local_path(upload.filename))
    Core.update_mural(mural, %{"image_path" => "/uploads/#{upload.filename}"})
    redirect(conn, to: "/murals/#{id}")
  end
  def upload_image(conn, %{"id" => id}) do
    conn
    |> put_flash(:error, "No image received")
    |> redirect(to: "/murals/#{id}")
  end

  def local_path(name) do
    case Application.get_env(:callahanartmap, :env) do
      :dev ->"priv/static/uploads/#{name}"
      _ ->
        Path.join([Application.app_dir(:callahanartmap), "priv/static/uploads/#{name}"])
    end
  end

  def index(conn, _params) do
    murals = Core.list_murals()
    render(conn, "index.html", murals: murals)
  end

  def new(conn, _params) do
    changeset = Core.change_mural(%Mural{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"mural" => mural_params}) do
    case Core.create_mural(mural_params) do
      {:ok, mural} ->
        conn
        |> put_flash(:info, "Mural created successfully.")
        |> redirect(to: mural_path(conn, :show, mural))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    mural = Core.get_mural!(id)
    cond do
      is_nil(mural.lat) ->
        case Core.geocode(mural) do
          {:ok, updated_mural} -> render(conn, "show.html", mural: updated_mural)
          _bad ->
            conn
            |> put_flash(:error, "Could not geocode mural address")
            |> render("show.html", mural: mural)
        end
      true -> render(conn, "show.html", mural: mural)
    end
  end

  def edit(conn, %{"id" => id}) do
    mural = Core.get_mural!(id)
    changeset = Core.change_mural(mural)
    render(conn, "edit.html", mural: mural, changeset: changeset)
  end

  def update(conn, %{"id" => id, "mural" => mural_params}) do
    mural = Core.get_mural!(id)

    case Core.update_mural(mural, mural_params) do
      {:ok, mural} ->
        conn
        |> put_flash(:info, "Mural updated successfully.")
        |> redirect(to: mural_path(conn, :show, mural))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", mural: mural, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    mural = Core.get_mural!(id)
    {:ok, _mural} = Core.delete_mural(mural)

    conn
    |> put_flash(:info, "Mural deleted successfully.")
    |> redirect(to: mural_path(conn, :index))
  end
end
